var http = require('http');
var express = require('express');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser')
var server = http.createServer(app);
var request = require('request')
var session = require('express-session');
var home_url = 'localhost'
var code_url='https://api.instagram.com/oauth/authorize/?'+
			'client_id=84481f939e1c4807a15024b6e745a54a&'+
			'redirect_uri=http://cheertour.csie.org:8080/get_code&'+
			'response_type=code';
var db_file = "./user.db";
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(db_file);
db.serialize(function() {
	db.run("CREATE TABLE IF NOT EXISTS user (id TEXT PRIMARY KEY,token TEXT)");
});

public_dir = 'public'

app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(session({secret: 'qqqqssssffffhhhh',cookie: { maxAge: (60000 * 24 * 30)}}));

var is_login = false;
app.get('/',function(req, res){
	console.log('index');
	sess=req.session
	if(sess.username)
	{
		//fs.createReadStream(public_dir+'/index.html').pipe(res);
		console.log('exist')
		res.render('index.html',{is_login:true})
	}
	else
	{
		res.render('index.html',{is_login:false})
	}
});

app.get('/login',function(req, res){
	console.log('login');
	sess=req.session
	if(!sess.username)
	{
		res.redirect(code_url);
	}
	else
	{
		res.redirect('/')
	}
});

app.get('/get_code',function(req,res){
	console.log('get_code')
	sess=req.session
	var code = req.param('code')
	request.post(
		'https://api.instagram.com/oauth/access_token',
		{form:
			{ 
				client_id:'84481f939e1c4807a15024b6e745a54a',
				client_secret:'331afdda187044cfa2d07bf616657313',
				grant_type:'authorization_code',
				redirect_uri:'http://cheertour.csie.org:8080/get_code',
				code:code
			} 
		},
		function (error, response, body) {
			if (!error && response.statusCode == 200) {
				re = JSON.parse(body)
				console.log(re.user.id)
				sess.username = re.user.id
				db.run("CREATE TABLE IF NOT EXISTS user (id TEXT,token TEXT)");
				var stmt = db.prepare("INSERT OR IGNORE INTO user (id,token) VALUES (?,?)");
				stmt.run([re.user.id,re.access_token]);
				stmt.finalize();
				res.redirect('/');
			}
		}
	);
})

app.post('/acc_checked',function(req,res){
	console.log(req.body);
	data =req.body;
	console.log(data.account);
	var msg=''
	db.each("SELECT name,token FROM user WHERE (user.name = '"+data.account+"');"
			,function(err, row) {
			msg=msg+row.name+','
			},function() {
			res.writeHead(200, {"Content-Type": "text/html"});
			if(msg.length>0)
				res.end('success')
			else
				res.end('fail')
	});
})

app.post('/sign', function(req,res){
	console.log(req.body);
	data =req.body;
	console.log(data.passwd);
	var msg=''
	var stmt = db.prepare("INSERT INTO user (name,passwd) VALUES (?,?)");
	stmt.run([data.account,data.passwd]);
	stmt.finalize();
	res.writeHead(200, {"Content-Type": "text/html"});
	res.end('success')
});

server.listen(8080,function(){
    console.log('localhost');
});
